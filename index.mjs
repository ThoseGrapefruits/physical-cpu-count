import cp from 'child_process';
import os from 'os';
import util from 'util';

const execPromise = util.promisify(cp.exec);

async function exec(command) {
  let { stdout } = await execPromise(command, { encoding: 'utf8' });
  return stdout;
}

const platform = os.platform();

let amount;

if (platform === 'linux') {
  const output =
    await exec('lscpu -p | egrep -v "^#" | sort -u -t, -k 2,4 | wc -l');
  amount = parseInt(output.trim());
} else if (platform === 'darwin') {
  const output = await exec('sysctl -n hw.physicalcpu_max');
  amount = parseInt(output.trim());
} else if (platform === 'win32') {
  const output = await exec('WMIC CPU Get NumberOfCores');
  amount = output.split(os.EOL)
    .map(function parse(line) { return parseInt(line); })
    .filter(function numbers(value) { return !isNaN(value); })
    .reduce(function add(sum, number) { return sum + number; }, 0);
} else {
  const cores = os.cpus().filter((cpu, index) => {
    const hasHyperthreading = cpu.model.includes('Intel');
    const isOdd = index % 2 === 1;
    return !hasHyperthreading || isOdd;
  });
  amount = cores.length;
}

export default amount;

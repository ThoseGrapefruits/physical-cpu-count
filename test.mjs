import assert from 'assert';
import cp from 'child_process';
import os from 'os';
import util from 'util';

import physicalCpuCount from './index.mjs';

const execPromise = util.promisify(cp.exec);

const commands = {
  darwin: 'sysctl -n hw.physicalcpu_max',
  linux: 'lscpu -p | egrep -v "^#" | sort -u -t, -k 2,4 | wc -l',
  win32: 'WMIC CPU Get NumberOfCores | more +1' // might fail on multi-CPU
};

const command = commands[os.platform()];
const { stdout: output } = await execPromise(command, { encoding: 'utf8' });
const expected = parseInt(output.trim(), 10);
const actual = physicalCpuCount;

assert.equal(
  actual,
  expected,
  'Should match OS reported number of physical cores ' +
  '(actual: ' + actual + ', ' +
  'expected: ' + expected + ')'
);
